<?php
namespace Freeway\Contract\Admin\Enum;

use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * admin用户状态枚举
 */
class AdminStatusEnum extends AbstractEnum
{
    /**
     * 正常
     */
    const ACTIVE = 1;

    /**
     * 禁用
     */
    const INACTIVE = 2;
}
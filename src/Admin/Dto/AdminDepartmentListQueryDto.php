<?php


namespace Freeway\Contract\Admin\Dto;

use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Dto\Pager\PagerListQueryDto;

class AdminDepartmentListQueryDto extends PagerListQueryDto
{
    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

}
<?php


namespace Freeway\Contract\Admin\Dto;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class AdminDepartmentCreationDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $password = null;

    /**
     * 用户姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * 备注
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $note = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('name', [
            new NotNull(['message' => '部门名字不能为空'])
        ]);
    }
}
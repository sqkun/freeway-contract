<?php


namespace Freeway\Contract\Admin\Dto;

use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Admin\Enum\AdminStatusEnum;
use Settlement\Contract\Admin\Enum\UserStatusEnum;
use Settlement\Contract\Common\Dto\Pager\PagerListQueryDto;



class AdminListQueryDto extends PagerListQueryDto
{
    /**
     * 姓名
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $username = null;


    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;


    /**
     * @var int|null
     * @see AdminStatusEnum
     * @Serializer\Type("int")
     */
    public ?int $status = null;

}
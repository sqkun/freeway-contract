<?php


namespace Freeway\Contract\Admin\Dto;


use Freeway\Contract\Admin\Model\AdminDepartmentModel;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Dto\Pager\PagerListDto;

class AdminDepartmentListDto extends PagerListDto
{

    /**
     * @var AdminDepartmentModel[]
     * @Serializer\Type("array<Settlement\Contract\Admin\Model\AdminDepartmentModel>")
     */
    public array $list = [];
}
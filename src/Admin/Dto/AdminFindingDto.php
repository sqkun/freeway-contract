<?php


namespace Freeway\Contract\Admin\Dto;


use JMS\Serializer\Annotation as Serializer;

class AdminFindingDto
{

    /**
     * 用户ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 用户手机号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    /**
     * 用户邮箱
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;
}
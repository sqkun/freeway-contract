<?php


namespace Freeway\Contract\Admin\Dto;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class AdminCreationDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $password = null;

    /**
     * 用户姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $username = null;

    /**
     * 用户电话
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    public static function assert(ClassMetadata $metadata)
    {

        $metadata->addPropertyConstraints('username', [
            new NotNull(['message' => '用户姓名不能为空'])
        ]);
        $metadata->addPropertyConstraints('mobile', [
            new NotNull(['message' => '用户手机号不能为空'])
        ]);

        $metadata->addPropertyConstraints('password', [
            new NotBlank([
                'message' => '密码不能为空'
            ]),
            new Length([
                'min' => 6,
                'max' => 20,
                'minMessage' => '密码不能少于6个字符',
                'maxMessage' => '密码不能多于20个字符',
            ])
        ]);

    }
}
<?php


namespace Freeway\Contract\Admin\Dto;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Admin\Model\AdminModel;
use Settlement\Contract\Common\Dto\Pager\PagerListDto;

class AdminListDto extends PagerListDto
{

    /**
     * @var AdminModel[]
     * @Serializer\Type("array<Settlement\Contract\Admin\Model\AdminModel>")
     */
    public array $list = [];
}
<?php


namespace Freeway\Contract\Admin\Dto;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Admin\Model\AdminModel;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class AdminUpdateDto extends AdminCreationDto
{

    /**
     * 自增ID
     *
     * @var int|null
     * @see UserModel::$id
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('id', [
            new NotNull(['message' => '用户ID不能为空'])
        ]);

        parent::assert($metadata);
    }
}
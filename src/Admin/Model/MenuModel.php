<?php

namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 菜单模型
 */
class MenuModel
{
    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $parentId = null;

    /**
     * 菜单名称
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $title = null;

    /**
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $icon = null;

    /**
     * 名字
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $sort = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $level = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $hidden = null;

    /**
     * 创建时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createTime = null;
}
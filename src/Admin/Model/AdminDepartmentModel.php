<?php

namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

class AdminDepartmentModel
{
    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * 备注
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $note = null;

    /**
     * 注册时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createTime = null;

    /**
     * 删除时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $deleteTime = null;
}
<?php

namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 资源模型
 */
class ResourceModel
{
    /**
     * ID
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 名字
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * 资源路径
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $url = null;

    /**
     * 描述
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $description = null;


    /**
     * ID
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $categoryId = null;

    /**
     * 创建时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createTime = null;
}
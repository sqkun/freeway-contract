<?php


namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Admin\Enum\AdminStatusEnum;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 用户
 */
class AdminModel
{
    /**
     * ID
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $username = null;

    /**
     * 头像
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $icon = null;

    /**
     * 昵称
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $nickName = null;

    /**
     * 手机号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    /**
     * 邮箱
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;

    /**
     * 密码
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $password = null;

    /**
     * Salt
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $note = null;

    /**
     * 状态
     * @var int|null
     * @see AdminStatusEnum
     * @Serializer\Type("int")
     */
    public ?int $status = null;

    /**
     * 部门
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $departmentId = null;

    /**
     * 注册时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createTime = null;

    /**
     * 最近登陆时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $loginTime = null;

    /**
     * 删除时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $deleteTime = null;

}
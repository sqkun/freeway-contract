<?php

namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 角色资源关联关系
 */
class RoleResourceRelationModel
{
    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;
    
    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $resourceId = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $roleId = null;
}
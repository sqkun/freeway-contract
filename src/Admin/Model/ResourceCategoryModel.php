<?php

namespace Freeway\Contract\Admin\Model;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 资源分类
 */
class ResourceCategoryModel
{
    /**
     * ID
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 名字
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $sort = null;

    /**
     * 创建时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createTime = null;
}
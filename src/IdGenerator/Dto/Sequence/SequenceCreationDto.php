<?php


namespace Settlement\Contract\IdGenerator\Dto\Sequence;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class SequenceCreationDto implements AssertionInterface
{

    /**
     * 序列名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * 序列初始值
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $value = null;

    /**
     * 步进
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $step = null;

    /**
     * hash salt
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $hashSalt = null;

    /**
     * hashId最小长度
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $hashMinLength = null;

    /**
     * hash字母表
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $hashAlphabet = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('name', [
            new NotBlank(['message' => '序列名不能为空'])
        ]);

        $metadata->addPropertyConstraints('step', [
            new Positive(['message' => '步进值必须是正整数'])
        ]);

        $metadata->addPropertyConstraints('hashSalt', [
            new Length([
                'max' => 20,
                'maxMessage' => 'Hash Salt最多20个字符'
            ])
        ]);

        $metadata->addPropertyConstraints('hashAlphabet', [
            new Length([
                'max' => 64,
                'maxMessage' => 'Hash字母表最多64个字符'
            ])
        ]);
    }
}
<?php


namespace Settlement\Contract\IdGenerator\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 序列
 */
class SequenceModel
{

    /**
     * 序列名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * 序列当前值
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $value = null;

    /**
     * 步进
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $step = null;

    /**
     * hash salt
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $hashSalt = null;

    /**
     * hashId最小长度
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $hashMinLength = null;

    /**
     * hash字母表
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $hashAlphabet = null;

    /**
     * 创建时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;

    /**
     * 更新时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $updatedAt = null;
}
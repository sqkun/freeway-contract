<?php


namespace Settlement\Contract\IdGenerator\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

class SequenceIdModel
{

    /**
     * 数字ID
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $id = null;

    /**
     * Hash ID
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $hashId = null;

    /**
     * 生成时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;
}
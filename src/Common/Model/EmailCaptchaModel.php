<?php


namespace Settlement\Contract\Common\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * 邮件验证码
 */
class EmailCaptchaModel
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $code = null;

    /**
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;

    /**
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $expireAt = null;
}
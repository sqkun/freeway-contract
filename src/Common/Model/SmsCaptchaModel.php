<?php


namespace Settlement\Contract\Common\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

class SmsCaptchaModel
{

    /**
     * ID
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 手机号
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    /**
     * 验证码
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $code = null;

    /**
     * 过期时间
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $expireAt = null;

    /**
     * 创建时间
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;
}
<?php


namespace Settlement\Contract\Common\Model;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Enum\AreaTypeEnum;

/**
 * 地区
 */
class AreaModel
{

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $parentId = null;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $weight = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $language = null;

    /**
     * @var int|null
     * @see AreaTypeEnum
     * @Serializer\Type("int")
     */
    public ?int $type = null;
}
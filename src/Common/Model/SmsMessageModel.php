<?php


namespace Settlement\Contract\Common\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Enum\SmsMessageStatusEnum;
use Tiny\Component\Mvc\ORM\Annotation\Column;

/**
 * SMS短信消息
 */
class SmsMessageModel
{

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $templateId = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $signature = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $body = null;

    /**
     * @var array|null
     * @Serializer\Type("array")
     * @Column(type="json")
     */
    public ?array $context = null;

    /**
     * @var int|null
     * @see SmsMessageStatusEnum
     * @Serializer\Type("int")
     */
    public ?int $status = null;

    /**
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;
}
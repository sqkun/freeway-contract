<?php


namespace Settlement\Contract\Common\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Tiny\Component\Mvc\ORM\Annotation\Column;

class FileModel
{

    /**
     * ID
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * 文件key
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $key = null;

    /**
     * 文件MD5
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $fileMd5 = null;

    /**
     * 文件名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $fileName = null;

    /**
     * 文件大小
     *
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $fileSize = null;

    /**
     * 文件MIME类型
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $fileMimeType = null;

    /**
     * 文件元数据
     *
     * @var array|null
     * @Serializer\Type("array")
     * @Column(type="json")
     */
    public ?array $fileMetadata = null;

    /**
     * 创建时间
     *
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;

}
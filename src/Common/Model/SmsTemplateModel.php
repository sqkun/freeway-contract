<?php


namespace Settlement\Contract\Common\Model;


use JMS\Serializer\Annotation as Serializer;

/**
 * 短信模板
 */
class SmsTemplateModel
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $id = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $body = null;
}
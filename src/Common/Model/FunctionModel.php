<?php


namespace Settlement\Contract\Common\Model;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Enum\FunctionTypeEnum;
use Tiny\Component\Mvc\ORM\Annotation\Column;
/**
 * 地区
 */
class FunctionModel
{

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $id = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public ?int $parentId = null;


    /**
     * @var int|null
     * @see FunctionTypeEnum
     * @Serializer\Type("int")
     */
    public ?int $type = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $serial = null;

    /**
     * 创建时间
     * @var DateTime|null
     * @Serializer\Type("DateTime")
     * @Column(type="datetime")
     */
    public ?DateTime $createdAt = null;
}

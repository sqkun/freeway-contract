<?php


namespace Settlement\Contract\Common\Dto\Func;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Dto\Pager\PagerListQueryDto;
class FunctionListQueryDto extends PagerListQueryDto
{


    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $serial = null;

}

<?php


namespace Settlement\Contract\Common\Dto\Func;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Model\FunctionModel;

class FunctionTreeDto extends FunctionModel
{

    /**
     * @var FunctionTreeDto[]
     * @Serializer\Type("array<Settlement\Contract\Common\Dto\Func\FunctionTreeDto>")
     */
    public ?array $children = null;
}

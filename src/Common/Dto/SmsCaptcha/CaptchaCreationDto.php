<?php


namespace Settlement\Contract\Common\Dto\SmsCaptcha;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class CaptchaCreationDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('mobile', [
            new NotBlank(['message' => '手机号不能为空'])
        ]);
    }
}
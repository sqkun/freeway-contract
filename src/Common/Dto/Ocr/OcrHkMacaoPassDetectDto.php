<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use GuzzleHttp\Psr7\UploadedFile;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Tiny\Component\Mvc\Validator\Constraint\PsrUploadedFile;

class OcrHkMacaoPassDetectDto implements AssertionInterface
{

    /**
     * 二进制文件
     *
     * @var UploadedFile|null
     * @Serializer\Type("GuzzleHttp\Psr7\UploadedFile")
     */
    public ?UploadedFile $file = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('file', [
            new NotBlank(['message' => '文件不能为空']),
            new PsrUploadedFile([
                'maxSize' => 10 * 1024 * 1024,
                'maxSizeMessage' => '文件大小不能超过10M'
            ])
        ]);
    }

}
<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Dto\File\FileDetailDto;

class OcrIdCardDetailDto
{

    /**
     * 姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = '';

    /**
     * 身份证号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $idCardNo = '';

    /**
     * 有效期开始时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $startDate = null;

    /**
     * 有效期结束时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $endDate = null;

    /**
     * 出生年月
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $birthday = '';

    /**
     * 裁剪后的图片
     *
     * @var FileDetailDto
     * @Serializer\Type("Settlement\Contract\Common\Dto\File\FileDetailDto")
     */
    public ?FileDetailDto $cropFile = null;

    /**
     * 地址
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $address = '';
}
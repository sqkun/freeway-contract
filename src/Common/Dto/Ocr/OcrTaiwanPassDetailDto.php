<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use JMS\Serializer\Annotation as Serializer;

class OcrTaiwanPassDetailDto
{

    /**
     * 中文名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $cnName = null;

    /**
     * 英文名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $enName = null;

    /**
     * 出生年月
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $birthday = null;

    /**
     * 性别
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $gender = null;

    /**
     * 有效期
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $expiryDate = null;

    /**
     * 有效期开始时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $startDate = null;

    /**
     * 有效期截止时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $endDate = null;

    /**
     * 签发机关
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $authority = null;

    /**
     * 签发地点
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $place = null;

    /**
     * 证件号码
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $cardNo = null;

    /**
     * 换证次数
     * @var string|null
     */
    public ?string $changeNum = null;

}
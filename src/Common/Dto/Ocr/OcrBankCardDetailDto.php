<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use JMS\Serializer\Annotation as Serializer;

class OcrBankCardDetailDto
{

    /**
     * 银行卡号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $bankCardNo = '';

    /**
     * 身份证号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $bankName = '';


}
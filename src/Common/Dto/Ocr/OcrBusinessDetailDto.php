<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use JMS\Serializer\Annotation as Serializer;

class OcrBusinessDetailDto
{

    /**
     * 详细地址
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $address = null;

    /**
     * 注册资本
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $registeredCapital = null;

    /**
     * 法人姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $legalPersonName = null;

    /**
     * 注册时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $registeredDate = null;

    /**
     * 营业执照名称
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $businessName = null;

    /**
     * 营业执照开始时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $businessStartAt = null;

    /**
     * 营业执照截止时间
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $businessEndAt = null;

    /**
     * 社会信用代码
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $socialCreditCode = null;

}
<?php


namespace Settlement\Contract\Common\Dto\Ocr;


use GuzzleHttp\Psr7\UploadedFile;
use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Enum\OcrIdCardSideEnum;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Tiny\Component\Mvc\Validator\Constraint\PsrUploadedFile;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Choice;

class OcrIdCardDetectDto implements AssertionInterface
{

    /**
     * 二进制文件
     *
     * @var UploadedFile|null
     * @Serializer\Type("GuzzleHttp\Psr7\UploadedFile")
     */
    public ?UploadedFile $file = null;

    /**
     * 正面/反面
     *
     * @var string|null
     * @see OcrIdCardSideEnum
     * @Serializer\Type("string")
     */
    public ?string $side = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('file', [
            new NotBlank(['message' => '文件不能为空']),
            new PsrUploadedFile([
                'maxSize' => 10 * 1024 * 1024,
                'maxSizeMessage' => '文件大小不能超过10M'
            ])
        ]);

        $metadata->addPropertyConstraints('side', [
            new NotNull([
                'message' => '识别类型不能为空'
            ]),
            new Choice([
                'choices' => OcrIdCardSideEnum::toArray(),
                'message' => '不支持的识别类型'
            ])
        ]);

    }

}
<?php


namespace Settlement\Contract\Common\Dto\IdCardVerification;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Validation\Constraint\IdNoConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class IdCardVerificationDto implements AssertionInterface
{

    /**
     * 身份证号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $idNo = null;

    /**
     * 姓名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $name = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('idNo', [
            new NotBlank(['message' => '身份证号不能为空']),
            new IdNoConstraint()
        ]);
        $metadata->addPropertyConstraints('name', [
            new NotBlank(['message' => '姓名不能为空'])
        ]);
    }
}
<?php


namespace Settlement\Contract\Common\Dto\BankCardInfo;


use JMS\Serializer\Annotation as Serializer;

class BankCardInfoDetailDto
{

    /**
     * 地区
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $area = null;

    /**
     * 银行卡产品名称
     *
     * @var string|null
     * @example 民生借记卡(银联卡)
     * @Serializer\Type("string")
     */
    public ?string $brand = null;

    /**
     * 银行名称
     *
     * @var string|null
     * @example 中国民生银行（03050000）
     * @Serializer\Type("string")
     */
    public ?string $bankName = null;

    /**
     * 银行卡种
     *
     * @var string|null
     * @example 借记卡
     * @Serializer\Type("string")
     */
    public ?string $cardType = null;

    /**
     * 银行官网
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $url = null;

    /**
     * 银行卡号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $cardNum = null;

    /**
     * 规范化的银行名称
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $formatBankName = null;


}
<?php


namespace Settlement\Contract\Common\Dto\BankCardInfo;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BankCardInfoParseDto implements AssertionInterface
{

    /**
     * 银行卡号
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $bankCardNo = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('bankCardNo', [
            new NotBlank(['message' => '银行卡号不能为空'])
        ]);

    }

}
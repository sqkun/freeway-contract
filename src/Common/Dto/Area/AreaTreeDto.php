<?php


namespace Settlement\Contract\Common\Dto\Area;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Model\AreaModel;

class AreaTreeDto extends AreaModel
{

    /**
     * @var AreaTreeDto[]
     * @Serializer\Type("array<Settlement\Contract\Common\Dto\Area\AreaTreeDto>")
     */
    public ?array $children = null;
}
<?php


namespace Settlement\Contract\Common\Dto\SmsMessage;


use JMS\Serializer\Annotation as Serializer;
use Settlement\Contract\Common\Validation\Constraint\MobileConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class SmsMessageCreationDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $templateId = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $mobile = null;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $signName = null;

    /**
     * @var array|null
     * @Serializer\Type("array")
     */
    public ?array $context = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('templateId', [
            new NotBlank(['message' => '短信模板ID不能为空']),
        ]);

        $metadata->addPropertyConstraints('mobile', [
            new NotBlank(['message' => '手机号不能为空']),
            new MobileConstraint()
        ]);
    }
}
<?php


namespace Settlement\Contract\Common\Dto\File;


use Settlement\Contract\Common\Model\FileModel;
use JMS\Serializer\Annotation as Serializer;

class FileDetailDto extends FileModel
{

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $url = '';
}
<?php


namespace Settlement\Contract\Common\Dto\File;


use GuzzleHttp\Psr7\UploadedFile;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;
use Tiny\Component\Mvc\Validator\Constraint\PsrUploadedFile;

class FileUploadDto implements AssertionInterface
{

    /**
     * 二进制文件
     *
     * @var UploadedFile|null
     * @Serializer\Type("GuzzleHttp\Psr7\UploadedFile")
     */
    public ?UploadedFile $file = null;

    /**
     * 远程文件
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $url = null;

    /**
     * base64编码的文件
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $encodedData = null;

    /**
     * 文件名
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $fileName = null;

    /**
     * @var array|null
     * @Serializer\Type("array")
     */
    public ?array $fileMetadata = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('file', [
            new PsrUploadedFile([
                'maxSize' => 10 * 1024 * 1024,
                'maxSizeMessage' => '文件大小不能超过10M'
            ])
        ]);

        $metadata->addConstraint(
            new Callback([self::class, 'validate'])
        );
    }

    public static function validate(FileUploadDto $object,
                                    ExecutionContextInterface $context)
    {
        if (empty($object->file) &&
            empty($object->url) &&
            empty($object->encodedData)) {
            $context->buildViolation('"file/url/encodedData"必须传一个')
                ->addViolation();
        }
    }
}
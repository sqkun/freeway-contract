<?php


namespace Settlement\Contract\Common\Dto\Pager;

use JMS\Serializer\Annotation as Serializer;

class PagerListDto
{

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public int $total = 0;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public array $list = [];

}
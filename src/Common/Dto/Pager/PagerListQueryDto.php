<?php


namespace Settlement\Contract\Common\Dto\Pager;


use JMS\Serializer\Annotation as Serializer;

class PagerListQueryDto
{

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $page = 1;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $limit = 10;

}
<?php


namespace Settlement\Contract\Common\Dto\EmailCaptcha;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;

class EmailCaptchaCreationDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $email = null;

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('email', [
            new NotBlank(['message' => '邮箱不能为空']),
            new Email(['message' => '邮箱格式错误'])
        ]);
    }
}
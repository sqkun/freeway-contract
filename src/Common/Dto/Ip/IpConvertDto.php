<?php


namespace Settlement\Contract\Common\Dto\Ip;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Tiny\Component\Mvc\Validator\AssertionInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class IpConvertDto implements AssertionInterface
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $ip = '';

    public static function assert(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('ip', [
            new NotBlank(['message' => 'ip地址不能为空'])
        ]);
    }

}
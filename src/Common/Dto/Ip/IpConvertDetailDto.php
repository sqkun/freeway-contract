<?php


namespace Settlement\Contract\Common\Dto\Ip;


use JMS\Serializer\Annotation as Serializer;

class IpConvertDetailDto
{

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public ?string $address = '';

}
<?php


namespace Settlement\Contract\Common\Validation\Validator;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IdNoValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if (null === $value || '' === $value) {
            return;
        }

        if (!preg_match(
            '/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d' .
            '{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|' .
            '(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

}
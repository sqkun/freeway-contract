<?php


namespace Settlement\Contract\Common\Validation\Constraint;


use Settlement\Contract\Common\Validation\Validator\IdNoValidator;
use Symfony\Component\Validator\Constraint;

class IdNoConstraint extends Constraint
{

    public string $message = '"{{ string }}" 不是有效的身份证号码';

    public function validatedBy()
    {
        return IdNoValidator::class;
    }
}
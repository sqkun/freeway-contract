<?php


namespace Settlement\Contract\Common\Validation\Constraint;


use Settlement\Contract\Common\Validation\Validator\MobileValidator;
use Symfony\Component\Validator\Constraint;

class MobileConstraint extends Constraint
{

    public string $message = '"{{ string }}" 不是有效的手机号';

    public function validatedBy()
    {
        return MobileValidator::class;
    }
}
<?php


namespace Settlement\Contract\Common\Validation\Constraint;


use Settlement\Contract\Common\Validation\Validator\BankCardNoValidator;
use Symfony\Component\Validator\Constraint;

class BankCardNoConstraint extends Constraint
{

    public string $message = '"{{ string }}" 不是有效的银行卡号';

    public function validatedBy()
    {
        return BankCardNoValidator::class;
    }
}
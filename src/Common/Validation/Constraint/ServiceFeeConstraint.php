<?php


namespace Settlement\Contract\Common\Validation\Constraint;


use Settlement\Contract\Common\Validation\Validator\ServiceFeeValidator;
use Symfony\Component\Validator\Constraint;

class ServiceFeeConstraint extends Constraint
{

    public string $message = '服务费"{{ string }}"格式不正确';

    public function validatedBy()
    {
        return ServiceFeeValidator::class;
    }
}
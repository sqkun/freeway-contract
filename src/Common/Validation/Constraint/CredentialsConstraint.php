<?php


namespace Settlement\Contract\Common\Validation\Constraint;


use Settlement\Contract\Common\Validation\Validator\CredentialsValidator;
use Symfony\Component\Validator\Constraint;

class CredentialsConstraint extends Constraint
{

    public string $message = '"{{ string }}" 不是有效的证件号码';

    public function validatedBy()
    {
        return CredentialsValidator::class;
    }
}
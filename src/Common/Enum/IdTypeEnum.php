<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 证件类型枚举
 */
class IdTypeEnum extends AbstractEnum
{

    /**
     * 身份证
     */
    const ID_CARD = 1;

    /**
     * 港澳居民往来内地通行证
     */
    const HK_MACAO_PASS = 2;

    /**
     * 台湾居民往来内地通行证识别
     */
    const TAIWAN_PASS = 3;

    public static $map = [
        self::ID_CARD => '身份证',
        self::HK_MACAO_PASS => '港澳居民往来内地通行证',
        self::TAIWAN_PASS => '台湾居民往来内地通行证识别',
    ];
}
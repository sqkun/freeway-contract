<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 操作名称枚举
 */
class OperationLogCommonActionEnum extends AbstractEnum
{

    /**
     * 新增
     */
    const CREATE = 'create';

    /**
     * 删除
     */
    const DELETE = 'delete';

    /**
     * 更新
     */
    const UPDATE = 'update';

    /**
     * 查询
     */
    const SELECT = 'select';

}
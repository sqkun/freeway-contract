<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 地区类型枚举
 */
class AreaTypeEnum extends AbstractEnum
{

    /**
     * 大陆/洲
     */
    const CONTINENT = 1;

    /**
     * 国家
     */
    const COUNTRY = 2;

    /**
     * 省份/州
     */
    const PROVINCE = 3;

    /**
     * 城市
     */
    const CITY = 4;

    /**
     * 城区
     */
    const DISTRICT = 5;

    /**
     * 商圈
     */
    const BUSINESS = 6;
}
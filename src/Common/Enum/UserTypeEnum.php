<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 用户类型枚举
 */
class UserTypeEnum extends AbstractEnum
{

    /**
     * 企业用户
     */
    const COMPANY = 1;

    /**
     * 个人用户
     */
    const PERSON = 2;
}
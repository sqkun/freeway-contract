<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 身份证OCR识别类型
 */
class OcrIdCardSideEnum extends AbstractEnum
{

    /**
     * 正面
     */
    const FACE = 'face';

    /**
     * 背面
     */
    const BACK = 'back';
}
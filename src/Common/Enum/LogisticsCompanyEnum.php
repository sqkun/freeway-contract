<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 物流公司枚举
 */
class LogisticsCompanyEnum extends AbstractEnum
{

    /**
     * EMS
     */
    const EMS = 1;

    /**
     * 顺丰
     */
    const SF = 2;
}
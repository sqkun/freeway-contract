<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 短信消息状态
 */
class SmsMessageStatusEnum extends AbstractEnum
{

    /**
     * 待发送
     */
    const CREATED = 1;

    /**
     * 已发送
     */
    const SENT = 2;

    /**
     * 发送失败
     */
    const FAILED = 3;
}
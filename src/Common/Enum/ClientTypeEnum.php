<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 平台客户端类型枚举
 */
class ClientTypeEnum extends AbstractEnum
{

    /**
     * www.lucasgchr.cn
     */
    const WWW_WEBSITE = 'www_website';

    /**
     * jiesuan.lucasgchr.cn
     */
    const JIESUAN_WEBSITE = 'jiesuan_website';

    /**
     * shuiwu.lucasgchr.cn
     */
    const SHUIWU_WEBSITE = 'shuiwu_website';

    /**
     * admin.lucasgchr.cn
     */
    const ADMIN_WEBSITE = 'admin_website';

    /**
     * daili.lucasgchr.cn
     */
    const DAILI_WEBSITE = 'daili_website';

    /**
     * 麦哲伦小程序
     */
    const MZL_MINIAPP = 'mzl_miniapp';

    /**
     * biz-api.lucasgchr.cn
     */
    const BIZ_API = 'biz_api';

    /**
     * channel-api.lucasgchr.cn
     */
    const CHANNEL_API = 'channel_api';

    const MAP = [
        self::WWW_WEBSITE => 'www.lucasgchr.cn',
        self::JIESUAN_WEBSITE => 'jiesuan.lucasgchr.cn',
        self::SHUIWU_WEBSITE => 'shuiwu.lucasgchr.cn',
        self::ADMIN_WEBSITE => 'admin.lucasgchr.cn',
        self::DAILI_WEBSITE => 'daili.lucasgchr.cn',
        self::MZL_MINIAPP => '麦哲伦小程序',
        self::BIZ_API => 'biz-api.lucasgchr.cn',
        self::CHANNEL_API => 'channel-api.lucasgchr.cn',
    ];
}
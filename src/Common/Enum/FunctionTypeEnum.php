<?php


namespace Settlement\Contract\Common\Enum;


use Tiny\Component\Mvc\Schema\AbstractEnum;

/**
 * 地区类型枚举
 */
class FunctionTypeEnum extends AbstractEnum
{

    /**
     * 一级
     */
    const FIRST_FUNCTION = 1;

    /**
     * 二级
     */
    const SECOND_FUNCTION = 2;

    /**
     * 三级
     */
    const THIRD_FUNCTION = 3;

    /**
     * 四级
     */
    const FOURTH_FUNCTION = 4;
}
